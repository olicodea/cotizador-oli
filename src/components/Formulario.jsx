import { MARCAS, ANIOS, PLANES } from "../constants";
import { Fragment } from "react";
import useCotizador from "../hooks/useCotizador";
import Error from "./Error";

const Formulario = () => {
    const { handleChangeDatos, datos, error, setError, cotizarSeguro } =
        useCotizador();

    const handleSubmit = (e) => {
        e.preventDefault();
        if (Object.values(datos).includes("")) {
            setError("Todos los campos son obligatorios.");
            return;
        }
        setError("");

        cotizarSeguro();
    };

    return (
        <>
            {error && <Error />}
            <form action="" onSubmit={handleSubmit}>
                <div className="my-5">
                    <label className="block mb-3 font-bold text-gray-600 uppercase">
                        Marca
                    </label>
                    <select
                        name="marca"
                        className="w-full p-3 bg-white border border-gray-300 rounded-md"
                        onChange={(e) => handleChangeDatos(e)}
                        value={datos.marca}
                    >
                        <option value="">-- Selecciona marca --</option>
                        {MARCAS.map((marca) => (
                            <option key={marca.id} value={marca.id}>
                                {marca.nombre}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="my-5">
                    <label className="block mb-3 font-bold text-gray-600 uppercase">
                        Año
                    </label>
                    <select
                        name="anio"
                        className="w-full p-3 bg-white border border-gray-300 rounded-md"
                        onChange={(e) => handleChangeDatos(e)}
                        value={datos.anio}
                    >
                        <option value="">-- Selecciona año --</option>
                        {ANIOS.map((anio) => (
                            <option key={anio} value={anio}>
                                {anio}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="my-5">
                    <label className="block mb-3 font-bold text-gray-600 uppercase">
                        Elige un plan
                    </label>
                    <div className="flex gap-3 items-center">
                        {PLANES.map((plan) => (
                            <Fragment key={plan.id}>
                                <label htmlFor="">{plan.nombre}</label>
                                <input
                                    type="radio"
                                    name="plan"
                                    value={plan.id}
                                    onChange={(e) => handleChangeDatos(e)}
                                />
                            </Fragment>
                        ))}
                    </div>
                </div>

                <input
                    type="submit"
                    className="w-full bg-indigo-500 hover:bg-indigo-600 transition-colors text-white cursor-pointer p-3 uppercase font-bold rounded-md"
                    value="cotizar"
                />
            </form>
        </>
    );
};

export default Formulario;
