import { createContext, useState } from "react";
import {
    obtenerDiferenciaAnio,
    calcularMarca,
    calcularPlan,
    formatearDinero,
} from "../helpers";

const CotizadorContext = createContext();

const CotizadorProvider = ({ children }) => {
    const [datos, setDatos] = useState({
        marca: "",
        anio: "",
        plan: "",
    });
    const [error, setError] = useState("");
    const [cargando, setCargando] = useState(false);
    const [resultado, setResultado] = useState(0);

    const handleChangeDatos = (e) => {
        setDatos({
            ...datos,
            [e.target.name]: e.target.value,
        });
    };

    const cotizarSeguro = () => {
        //Una base
        let resultado = 2000;

        //Obtener diferencia de años
        const diferencia = obtenerDiferenciaAnio(datos.anio);

        //Hay que restar 3% por cada año
        resultado -= diferencia * 0.03 * resultado;

        //Americano 15%
        //Europeo 30%
        //Asiatico 5%
        resultado *= calcularMarca(datos.marca);

        //Basico 20%
        //Completo 50%
        resultado *= calcularPlan(datos.plan);

        resultado = resultado.toFixed(2);

        //Formatear dinero
        resultado = formatearDinero(resultado);

        setCargando(true);

        setTimeout(() => {
            setResultado(resultado);
            setCargando(false);
        }, 3000);
    };

    return (
        <CotizadorContext.Provider
            value={{
                handleChangeDatos,
                datos,
                error,
                setError,
                cotizarSeguro,
                resultado,
                setResultado,
                cargando,
            }}
        >
            {children}
        </CotizadorContext.Provider>
    );
};

export { CotizadorProvider };

export default CotizadorContext;
