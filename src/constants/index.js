export const MARCAS = [
    { id: 1, nombre: "Europea" },
    { id: 2, nombre: "Americano" },
    { id: 3, nombre: "Asíatico" },
];

const ANIOMAX = new Date().getFullYear();

export const ANIOS = Array.from(
    new Array(20),
    (valor, index) => ANIOMAX - index
);

export const PLANES = [
    { id: 1, nombre: "Básico" },
    { id: 2, nombre: "Completo" },
];
